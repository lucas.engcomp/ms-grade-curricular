package com.cliente.escola;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsGradeCurricularApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsGradeCurricularApplication.class, args);
    }

}
